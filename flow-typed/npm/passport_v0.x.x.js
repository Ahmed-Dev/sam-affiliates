// @flow

declare module passport {
  declare function use(...params : Array<any>): any;
}