declare type User = {
  name: string,
  email: string,
  affiliate_ids: Array<string>,
  password: string
};

declare module 'my-types' {
  declare type User = User;
  declare module.exports: {}
}