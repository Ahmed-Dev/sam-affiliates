with Views as (
  select 
    $[params.f_page('e', 'timestamp')]$ as page
  , $[params.f_section('e', 'timestamp')]$ as section
  , $[params.f_row('e', 'timestamp')]$ as row
  , sum(case when e.view then 1 else 0 end) :: int as views
  , sum(case when e.lead then 1 else 0 end) :: int as leads
  , sum(case when(e.sale_pixel_direct or e.sale_pixel_delayed) and (e.scrub is not true) then 1 else 0 end) :: int as sales
  , sum(case when e.firstbilling then 1 else 0 end) :: int as firstbillings
  , sum(case when e.optout then 1 else 0 end) :: int as optouts
  from public.events e 
  where e.timestamp >= $[params.from_date_tz]$
    and e.timestamp < $[params.to_date_tz]$
    and $[params.f_filter('e')]$
  group by page, section, row
  order by page, section, row
)

, Optout24 as (

  select 
    $[params.f_page('e', 'timestamp')]$ as page
  , $[params.f_section('e', 'timestamp')]$ as section
  , $[params.f_row('e', 'timestamp')]$ as row
  , sum(case when e.optout and e.active_duration is not null and e.active_duration <= 86400 then 1 else 0 end) :: int as optout_24
  from events e
  where e.timestamp >= $[params.from_date_tz]$
    and e.timestamp < dateadd(day, 1, $[params.to_date_tz]$)
    and $[params.f_filter('e')]$
  group by page, section, row
  order by page, section, row
)

select v.*, o.optout_24 from Views v
left join Optout24 o on v.page = o.page and v.section = o.section and v.row = o.row
