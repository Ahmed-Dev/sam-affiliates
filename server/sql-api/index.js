// @flow

const query = require('./sql-api')

const connection_strings = {
  helix_connection_string: process.env['helix_connection_string'],
  jewel_connection_string: process.env['jewel_connection_string'],
}

const respond = (connection_string: string, sql, params, res, map = x => x) => {
  query(connection_string, sql, params)
    .then(x => {
      res.set('Content-Type', 'text/json')
      res.set('Cache-Control', 'public, max-age=7200')
      res.end(JSON.stringify(map(x.rows)))
    })
    .catch(x => {
      console.error(x)
      res.status(500)
      res.end(`Error:\n${x.toString()}`)
    })
}

const query_with_connection_name = (
  connection_name: string,
  sql,
  params,
  map = x => x,
) =>
  new Promise((resolve, reject) => {
    const connection_string = connection_strings[connection_name]
    if (connection_string == null) {
      return reject(
        Error(`Error:\n${connection_name} env variable is not provided.`),
      )
    } else
      return query(connection_string, sql, params).then(result =>
        resolve(map(result.rows)),
      )
  })

const respond_with_connection_name = (
  connection_name: string,
  sql,
  params,
  res,
  map = x => x,
) => {
  const connection_string = connection_strings[connection_name]
  if (connection_string == null) {
    res.status(500)
    res.end(`Error:\n${connection_name} env variable is not provided.`)
  } else respond(connection_string, sql, params, res, map)
}

const query_helix = (sql: string, params: Object, map = x => x) =>
  query_with_connection_name('helix_connection_string', sql, params, map)
const query_jewel = (sql: string, params: Object, map = x => x) =>
  query_with_connection_name('jewel_connection_string', sql, params, map)

const respond_helix = (sql: string, params: Object, res, map = x => x) =>
  respond_with_connection_name('helix_connection_string', sql, params, res, map)
const respond_jewel = (sql: string, params: Object, res, map = x => x) =>
  respond_with_connection_name('jewel_connection_string', sql, params, res, map)

module.exports = {
  respond_helix,
  respond_jewel,
  query_helix,
  query_jewel,
}
