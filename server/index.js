const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const morgan = require('morgan')
const path = require('path')

app.use(express.static('dist'))
app.use(morgan('combined'))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.disable('x-powered-by')

const authenticate = require('./auth')(app)

require('./api')(app, authenticate)

app.use('/*', express.static('../dist'))

app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, '../dist', 'index.html'));
})

//port configuration for production environment
const port = process.env.PORT || 3081
app.listen(port, () => {
  console.log('We are live on ' + port)
})
