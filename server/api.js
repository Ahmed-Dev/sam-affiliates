// @flow

const { respond_jewel, query_jewel } = require('./sql-api')
const R = require('ramda')
const fs = require('fs')
import type {$AuthenticatedRequest, $Request, $Response, $Application, $Middleware} from 'express'

const filter_to_pipe_syntax = (x : string, rest : Array<Array<string>>  = []) =>
  R.pipe((x == '-'
    ? () => rest
    : R.pipe(
        R.split(','),
        R.map(R.pipe(R.split('='), R.map(x => x.trim()))),
        R.concat(rest)
      )
    ),
    R.map(R.join(',')),
    R.join(','),
  )(x)
      
module.exports = (app : $Application, authenticate : () => $Middleware) => {

  app.post('/api/v1/run_query', (req : $Request, res: $Response) => {
    // this route is for testing SQL connections. Do not use it in real-life!

    var body = ''
    req.setEncoding('utf8')

    req.on('data', function(chunk) {
      body += chunk
    })

    req.on('end', function() {
      // res.end(body)
      respond_jewel(body, {}, res)
    })
  })


  app.get(
    '/api/v1/sales/:timezone/:from_date/:to_date/:filter/:page/:section/:row',
    authenticate(),
    (req: $AuthenticatedRequest, res: $Response) => {
      if(!req.user.affiliate_ids || req.user.affiliate_ids.length == 0) {
        res.status(403)
        return res.json({error: `affiliate_ids length is 0 for this affiliate: ${!!req.user ? req.user.name : 'Unknown User Name'}`})
      } else {
        const params = R.merge(req.params, {
          filter: filter_to_pipe_syntax(req.params.filter, [['affiliate_id', (req.user.affiliate_ids || []).join(';')]]),
        })
        respond_jewel(
          fs.readFileSync('./server/sql-templates/sales/index.sql', 'utf8'),
          params,
          res,
          require('./sql-templates/sales')(params),
        )
      }
    },
  )

  query_jewel(
    `select * from affiliate_mapping`,
    {},
    R.pipe(
      R.groupBy(x => x.affiliate_name),
      R.map(R.map(x => x.affiliate_id)),
      R.toPairs,
      R.map(([affiliate_name, affiliate_ids]) => ({
        affiliate_name,
        affiliate_ids,
      })),
    ),
  )


  app.get('/api/v1', authenticate(), (req : $AuthenticatedRequest, res : $Response) => {
    const affiliateName = req.body.affiliate
    res.json({ affiliateName: 'affiliateName' })
  })
}
