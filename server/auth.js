// @flow

const jwt = require('jwt-simple')
const userModel = require('./userModel.js').user
const passport = require('passport')
const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt
const LocalStrategy = require('passport-local').Strategy

passport.serializeUser((user, callback) => callback(null, user.email))
passport.deserializeUser((email, callback) => {
  userModel.getUserByEmail(email, (err, obj) => {
    callback(err, obj)
  })
})

const token = user => {
  const timestamp = new Date().getTime()
  return jwt.encode({ email: user, createdAt: timestamp }, 'secret')
}

passport.use(
  new JwtStrategy(
    {
      jwtFromRequest: ExtractJwt.fromHeader('authorization'),
      secretOrKey: 'secret',
    },
    (payload, done) => {
      console.log('authentication started')
      userModel.getUserByEmail(payload.email, (err, obj) => {
        if (err) {
          return done(err)
        }
        if (!obj) {
          return done(null, false, { message: 'incorrect email!' })
        }
        return done(null, obj)
      })
    },
  ),
)

passport.use(
  new LocalStrategy(
    {
      usernameField: 'email',
    },
    (email, password, done) => {
      console.log('authentication started')
      userModel.getUserByEmail(email, (err, obj) => {
        if (err) {
          return done(err)
        }
        if (!obj) {
          return done(null, false, { message: 'incorrect email!' })
        }
        if (!userModel.compareUser(obj, password)) {
          return done(null, false, { message: 'incorrect password' })
        }
        return done(null, obj)
      })
    },
  ),
)


const requireAuth = passport.authenticate('jwt', { session: false })
const requireSignin = passport.authenticate('local', { failwitherror: true })

module.exports = app => {
  app.use(passport.initialize())
  app.use(passport.session())

  app.use((req, res, next) => {
    res.set('Access-Control-Allow-Origin', '*')
    res.set(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept',
    )
    next()
  })

  app.post(
    '/api/login',
    requireSignin,
    (req, res, next) => {
      console.log('Sucess!')
        const email = req.user.email
        const affiliateName = req.user.name
        console.log(affiliateName)
        res.end(
          JSON.stringify({
            success: true,
            affiliateName: affiliateName,
            email: email,
            token: token(email),
          }),
        )
    },
    (err, req, res, next) => {
      res.send(401)
    },
  )

  app.post(
    '/api/login-with-token',
    requireAuth,
    (req, res, next) => {
      // const token = req.body.token
      const affiliateName = req.body.affiliateName
      const email = req.body.email
      res.end(
          JSON.stringify({
            success: true,
            affiliateName: affiliateName,
            email: email,
            token: token(email),
          }),
        )
    },
    (err, req, res, next) => {
      res.send(401)
    },
  )

  app.get('/api/logout', requireAuth, (req, res) => {
    req.session.destroy()
    res.end(JSON.stringify({ session: 'destroyed' }))
  })

  return () => requireAuth
}
