# Sam Affiliates Portal

#### Installation
For development environment in the local machine:
```sh
$ git clone git@bitbucket.org:Ahmed-Dev/sam-affiliates.git
$ cd sam-affiliates
$ yarn install
$ npm run dev
```
### Continous Delivery to Herokuapp
With every commit pushed to this repositry, the build process for deployment starts through the pipline integration with herokuapp. The project is live at:
[https://sam-affiliates-portal.herokuapp.com](https://sam-affiliates-portal.herokuapp.com)

### API
|version| 1(current)|
|------- | ---------|
|Structure| /api/v1/:affiliate_id/:api_function/:from_date/:to_date/:filter/:page/:section/:row|
 