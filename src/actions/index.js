import { actionTypes as types, urls } from '../constants'
import { post, get } from '../helpers'
import { fetchState } from '../adts'

export const login = ({ email, password }) => dispatch => {
  dispatch({ type: types.LOGIN_REQUEST })
  post({
    url: urls.LOGIN,
    body: { email, password },
    success: types.LOGIN_SUCCESS,
    failure: types.LOGIN_FAILURE,
    dispatch,
  })
}

export const loginWithToken = () => (dispatch, getState) => {
  const user = getState().user
  if (!user) return

  const token = user.token
  console.log(token)
  const email = user.email
  const affiliateName = user.affiliateName

  if (typeof token === 'undefined') return

  dispatch({ type: types.LOGIN_REQUEST })
  post({
    url: urls.LOGIN_WITH_TOKEN,
    token,
    body: { email, affiliateName },
    success: types.LOGIN_SUCCESS,
    failure: types.LOGIN_FAILURE,
    dispatch,
  })
}

export const getHello = () => (dispatch, getState) => {
  console.log('get hello called!!')
  const user = getState().user
  const affiliateName = user.affiliateName
  const token = user.token
  if (typeof token === 'undefined') return
  dispatch({ type: types.GET_HELLO_REQUEST })
  get({
    url: urls.GET_HELLO + affiliateName,
    token,
    success: types.GET_HELLO_SUCCESS,
    failure: types.GET_HELLO_FAILURE,
    dispatch,
  })
}

export const getLogout = () => (dispatch, getState) => {
  console.log('get logout called!!')
  const user = getState().user.token
  if (typeof user !== 'undefined') return
  dispatch({ type: types.GET_LOGOUT_REQUEST })
  get({
    url: urls.GET_LOGOUT,
    success: types.GET_LOGOUT_SUCCESS,
    failure: types.GET_LOGOUT_FAILURE,
    dispatch,
  })
}

export const getSalesStats = () => (dispatch, getState) => { 
  const user = getState().user
  const token = user.token
  if (typeof token === 'undefined') return //TODO: handle th error
  fetchState.fetch(x => dispatch({ type: types.GET_SALES_STATS, payload: x }) , () => 
    fetch(urls.GET_SALES_STATS, {
      method: 'GET',
      headers: {
        authorization: token,
        'Content-Type': 'application/json',
      },
    }).then(x => x.json())
  )
}