export const post = async ({ url, token, body, success, failure, dispatch }) => {
  try {
    const res = await fetch(url, {
      method: 'POST',
      headers: {
        authorization: token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    })
    const data = await res.json()
    console.log(data)
    dispatch({ type: success, data })
  } catch (e) {
    const data = { sucess: false, failure: true }
    console.log(data)
    dispatch({ type: failure, data })
  }
}

export const get = async ({ url, token, success, failure, dispatch }) => {
  try {
    const res = await fetch(url, {
      method: 'GET',
      headers: {
        authorization: token,
        'Content-Type': 'application/json',
      },
    })
    const data = await res.json()
    console.log(data)
    dispatch({ type: success, data })
  } catch (e) {
    const data = { sucess: false, failure: true }
    console.log(data)
    dispatch({ type: failure, data })
  }
}
