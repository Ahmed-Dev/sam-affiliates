// @flow

import { combineReducers } from 'redux'

import { actionTypes as types } from '../constants'
import user from './user'
import getHello from './getHello'
import getLogout from './getHello'
import fetchBuilder from './fetchBuilder'

const rootReducer = combineReducers({
  logger: (state = null, action) => {
    console.log('action', action)
    return state
  },
  user,
  getHello,
  getLogout,
  salesStats: fetchBuilder(types.GET_SALES_STATS)
})

export default rootReducer
