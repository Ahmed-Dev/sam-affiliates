import { fetchState } from '../adts'

export default (type: string) => (state = fetchState.Nothing(), action) => {
  if(action.type == type) {
    return action.payload
  } else {
    return state
  }
}
