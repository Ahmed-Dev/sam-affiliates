import React from 'react'
import { connect } from 'react-redux'
import { Grid, Statistic } from 'semantic-ui-react'
import { getHello } from '../actions'
import PropTypes from 'prop-types'
import { format } from 'd3-format'

const Stats = ({ sales, cr }) => {
  return (
    <Grid columns={2} divided>
      <Grid.Row>
        <Grid.Column>
          <Statistic>
            <Statistic.Label>Sales</Statistic.Label>
            <Statistic.Value>{ format(',')(sales) }</Statistic.Value>
          </Statistic>
        </Grid.Column>
        <Grid.Column>
          <Statistic>
            <Statistic.Label>Conversion Rate</Statistic.Label>
            <Statistic.Value>{format('0.2%')(cr)}</Statistic.Value>
          </Statistic>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
}

Stats.propTypes = {
  sales: PropTypes.number.isRequired,
  cr: PropTypes.number.isRequired
}

export default Stats
