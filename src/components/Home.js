// @flow

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { Container, Grid, Header, Button } from 'semantic-ui-react'
import Stats from './Stats'
import { Blue } from './Styled'
import { store } from '../store'
import { getHello, getLogout, getSalesStats } from '../actions'
import { fetchState, match } from '../adts'
import type { FetchState } from '../adts'
const md = require('markdown-it')();

type HomeState = {
  mainMessage: FetchState<string>
}

class Home extends React.Component {
  state: HomeState
  constructor(props) {
    super(props)
    this.state = {
      mainMessage: fetchState.Nothing()
    }
  }
  componentWillMount() {
    store.dispatch(getHello())
    store.dispatch(getSalesStats())

    fetchState.fetch(x => this.setState({ mainMessage: x }), () => 
      fetch('/markdown/main-message.md').then(x => x.text()).then(x => md.render(x))
    )
  }
  logout() {
    //TODO: debug logout action to get desired response (async calls)
    window.localStorage.removeItem('state')
    store.dispatch(getLogout())
    window.location = ''
  }

  render() {
    return (
      <div>
        {this.props.user.token
          ? <Container>
              <Grid columns={3}>
                <Grid.Row>
                  <Grid.Column>
                    <Header>
                      {"You're logged in as "}
                      <Blue>{this.props.user.email}</Blue>
                    </Header>
                  </Grid.Column>
                  <Grid.Column>
                    <Header>
                      Affiliate Name: {this.props.user.affiliateName}
                    </Header>
                  </Grid.Column>
                  <Grid.Column>
                    <Button onClick={this.logout.bind(this)}>Logout</Button>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
              <Grid columns={1}>
                <Grid.Row>
                  <Grid.Column>
                  {
                    match({
                        Nothing: () => '-'
                      , Loading: () => 'Loading ...'
                      , Loaded: (html) => <div dangerouslySetInnerHTML={ { __html: html } } />
                      , Error: (err) => <div className='error'>{ err.toString() }</div>
                    })(this.state.mainMessage)
                  }
                  </Grid.Column>
                </Grid.Row>
              </Grid>
              {
                match({
                    Nothing: () => '-'
                  , Loading: () => 'Loading Saes Stats...'
                  , Error: (err) => <div className='error'>{ err.toString() }</div>
                  , Loaded: (stats) => <Stats sales={ !!stats[0] ? stats[0].sales : 0 } cr={ !!stats[0] && !!stats[0].views ? (stats[0].sales / stats[0].views) : 0 } />
                })(this.props.salesStats)
              }
            </Container>
          : <Redirect to="/login" />}
      </div>
    )
  }
}

Home.propTypes = {
  user: PropTypes.shape({}).isRequired,
  getHello: PropTypes.shape({}).isRequired,
  getLogout: PropTypes.shape({}).isRequired,
  salesStats: PropTypes.func.isRequired
}
export default connect(state => ({
  user: state.user,
  getHello: state.getHello,
  getLogout: state.getLogout,
  salesStats: state.salesStats
}))(Home)
